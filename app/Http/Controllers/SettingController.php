<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

use Validator;
use DB;
use App;
use Input;
use Hash;
use App\Models\UserMobile;
use App\Models\UserMobileList;
use App\Models\Listes;
use App\Models\Categories;
use App\Models\Group;

use Response;
use App\Models\MobileListe;
use App\Models\MobileGroup;
use App\Models\MobileGroupListe;
use App\Models\GroupMobile;
use App\Models\GroupUserMobile;
use App\Models\UserNum;

class SettingController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){
		$this->middleware('auth');
	}

	public function getGroup(){
	    $userMobile = UserMobile::orderBy('name')->get();

	    $groups = Group::orderBy('name')->get();
	    $groupsUser = [];
//	    if ( count($groups) > 0 ){
//	        foreach($groups as $g){
//	            $grouplist = UserNum::where('group_id', $g->id)->get();
//	            if ( count($grouplist) > 0 ){
//                    $groupsUser[$g->id] = $grouplist;
//                }
//            }
//        }
	    return view('setting.group', [
	        'userMobile' => $userMobile,
            'groups' => $groups,
            'groupsUser' => $groupsUser
        ]);
	}
	
	public function postLoadGroup(){
	    $user_mobile_id = Input::get('user_mobile_id');
        $group_id = Input::get('group_id');

        $groupUserList = GroupUserMobile::select('user_num.*')
            ->join('user_num', 'user_num.id', '=', 'groups_user_mobiles.user_num_id')
            ->where('groups_user_mobiles.user_mobile_id', $user_mobile_id)
            ->where('groups_user_mobiles.group_id', $group_id)
            ->get();

        $gulsed = [];
        if ( count($groupUserList) > 0){
            foreach($groupUserList as $gu){
                $gulsed[$gu->id] = $gu->id;
            }
        }
        $userNumload = UserNum::where('group_id', $group_id)->get();

        $views = view('setting.user-list', [
            'gulsed' => $gulsed,
            'userNumload' => $userNumload
        ])->render();

	    return Response::json(['views' => $views]);
	}


    public function postStoreGroup(){

        $user_mobile_id = Input::get('user_mobile_id');
        $groupUserId = Input::get('group_id');
        $userNum = Input::get('userNum');
        $userGroup = Input::get('userGroup');
        $userMobile = UserMobile::find($user_mobile_id);
        if ( $userMobile->types == 'group' ){
            $groupUserMobile = [];

            $groupUserList = GroupUserMobile::where('user_mobile_id', $userMobile->id)->groupBy('group_id')->get();
            if ( count($groupUserList) > 0 ){
                foreach($groupUserList as $gul){
                    $groupUserMobile[$gul->group_id] = $gul;
                }
            }

            foreach($userGroup as $gId){
                if ( !isset($groupUserMobile[$gId]) ){
                    $gum = new GroupUserMobile;
                    $gum->user_mobile_id = $user_mobile_id;
                    $gum->group_id = $gId;
                    $gum->user_num_id = 0;
                    $gum->save();
                }else{
                    unset($groupUserMobile[$gId]);
                }
            }

            if ( count($groupUserMobile) > 0){
                foreach($groupUserMobile as $gData){
                    $gData->delete();
                }
            }
        }elseif($userMobile->types == 'upload'){
            $groupUserList = GroupUserMobile::where('user_mobile_id', $userMobile->id)
                ->where('group_id', $groupUserId)
                ->get();
            if ( count( $groupUserList ) <= 1){
                if ( count( $groupUserList ) == 0 ){
                    $gum = new GroupUserMobile;
                }else{
                    $gum = $groupUserList[0];
                }
            }else if ( count( $groupUserList ) > 1 ){
                GroupUserMobile::where('user_mobile_id', $userMobile->id)
                    ->where('group_id', $groupUserId)
                    ->delete();

                $gum = new GroupUserMobile;
            }

            $gum->user_mobile_id = $userMobile->id;
            $gum->group_id = $groupUserId;
            $gum->user_num_id = $userNum;
            $gum->save();
        }
//        $groupUserMobile = [];

//        $groupUserList = GroupUserMobile::where('user_mobile_id', $user_mobile_id)->where('group_id', $groupUserId)->get();
//        if ( count($groupUserList) > 0 ){
//            foreach($groupUserList as $gul){
//                $groupUserMobile[$gul->user_num_id] = $gul;
//            }
//        }

//        foreach($userNum as $uId){
//            if ( !isset($groupUserMobile[$uId]) ){
//                $gum = new GroupUserMobile;
//                $gum->user_mobile_id = $user_mobile_id;
//                $gum->group_id = $groupUserId;
//                $gum->user_num_id = $uId;
//                $gum->save();
//            }else{
//                unset($groupUserMobile[$uId]);
//            }
//        }

//        if ( count($groupUserMobile) > 0){
//            foreach($groupUserMobile as $gData){
//                $gData->delete();
//            }
//        }
        return Response::json(['status' => 1]);
    }

	public function postStoreGroupBack(){

        $user_mobile_id = Input::get('user_mobile_id');
        $groupUserId = Input::get('group_user_id');

//        $groupMobile = [];
//        $groupList = GroupMobile::where('user_mobile_id', $user_mobile_id)->get();
//        if ( count($groupList) > 0 ){
//            foreach($groupList as $gl){
//                $groupMobile[$gl->group_id] = $gl;
//            }
//        }
//        foreach($groupId as $gi){
//            if ( !isset($groupMobile[$gi]) ){
//                $groupMob = new GroupMobile;
//                $groupMob->user_mobile_id = $user_mobile_id;
//                $groupMob->group_id = $gi;
//                $groupMob->save();
//            }else{
//                unset($groupMobile[$gi]);
//            }
//        }
//
//        if ( count($groupMobile) > 0 ){
//            foreach($groupMobile as $dgm){
//                $dgm->delete();
//            }
//        }

        $groupUserMobile = [];

        $groupUserList = GroupUserMobile::where('user_mobile_id', $user_mobile_id)->get();
        if ( count($groupUserList) > 0 ){
            foreach($groupUserList as $gul){
                $groupUserMobile[$gul->group_id][$gul->user_num_id] = $gul;
            }
        }

        foreach($groupUserId as $gId => $guData){
            foreach($guData as $uId => $uData){
                if ( !isset($groupUserMobile[$gId][$uId]) ){
                    $gum = new GroupUserMobile;
                    $gum->user_mobile_id = $user_mobile_id;
                    $gum->group_id = $gId;
                    $gum->user_num_id = $uId;
                    $gum->save();
                }else{
                    unset($groupUserMobile[$gId][$uId]);
                }
            }
        }

        if ( count($groupUserMobile) > 0){
            foreach($groupUserMobile as $gData){
                foreach($gData as $dgum){
                    $dgum->delete();
                }
            }
        }
	    return Response::json(['status' => 1]);
	}
	
	public function getSetupGroup(){	    
	    $groups = Categories::orderBy('title')->get();
	    return view('setting.setup-group', ['groups' => $groups]);
	}
	
	public function setupGroupLoad(){
	    $groupId = Input::get('group_id');
	    
	    $listeGroup = Listes::select('listes.*')
        ->join('percents', 'percents.liste_id', '=', 'listes.id')
        ->where('percents.group_id', $groupId)
        ->distinct()
        ->get();
	    
        $mobileGroupListeListe = MobileGroupListe::where('group_id', $groupId)->get();
        $mobileGroupListe = [];
       
        if ( count($mobileGroupListeListe) > 0 ){
           foreach($mobileGroupListeListe as $gl){
               $mobileGroupListe[$gl->liste_id][$gl->types] = $gl;
           }
        }
       
        $htmls = view('setting.setup-group-load', ['listeGroup' => $listeGroup, 'mobileGroupListe' => $mobileGroupListe])->render();
	    return Response::json(['htmls' => $htmls]);
	}
	
	public function setupGroupStore(){
	    $groupId = Input::get('group_id');
	    $listeGroupId = Input::get('listeGroup_id');
	    
	    $mobileGroupListeListe = MobileGroupListe::where('group_id', $groupId)->get();
	    $mobileGroupListe = [];
	    
	    if ( count($mobileGroupListeListe) > 0 ){
	        foreach($mobileGroupListeListe as $gl){
	            $mobileGroupListe[$gl->liste_id][$gl->types] = $gl;
	        }
	    }
	    
	    foreach($listeGroupId as $listeId => $data){
	        foreach($data as $types => $val){
	            if ( !isset($mobileGroupListe[$listeId][$types]) ){
	                $mobileGL = new MobileGroupListe;
	                $mobileGL->group_id = $groupId;
	                $mobileGL->liste_id = $listeId;
	                $mobileGL->types = $types;
	                $mobileGL->save();
	            }else{
	                unset($mobileGroupListe[$listeId][$types]);
	            }
	        }	        
	    }
	    
	    if ( count($mobileGroupListe) ){
	        foreach($mobileGroupListe as $dlisteId => $ddata){
	            foreach($ddata as $dtypes => $dval){
	                $dval->delete();
	            }
	        }
	    }
	}

	public function getGroupUser(){

    }
}
