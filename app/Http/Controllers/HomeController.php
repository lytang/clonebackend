<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

use Validator;
use DB;
use App;
use Input;
use Hash;

use Laravel\Passport\Passport;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth', ['except' => 'login']);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function login()
	{	       
		if ( Auth::guest() ){
			return view('login');
		}else{
			//return 'logged in';
			return Redirect::route('admin.index');
		}
		
	}
	

	public function logout(){
		Auth::logout();
		return Redirect::route('login');
	}

	public function ChangePassword(){				
		return view('change-password');
	}
	
	public function postChangePassword(){
		$validator = Validator::make(Input::all(), [
			'password' => 'required|min:3|same:password_confirm',
			'password_confirm' => 'required|min:3'
		]);
		
		if ($validator->fails()) {
			return Redirect::route('user.password')
			->withErrors($validator)
			->withInput();
		}
		
		$user = User::find(Auth::Id());
		
		$user->password = Hash::make(Input::get('password'));
		
		$user->save();
		
		return Redirect::route('user.password')->with('saveSuccess', 'Password has been change');
	}
}
