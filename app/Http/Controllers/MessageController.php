<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Validator;
use OneSignal;

// Use to get current date
use Carbon\Carbon;

// Models
use DB;
use App\Messages;
use App\Chat_rooms;
use App\Chat_paticipate;
use App\Delete_chat_room;
use App\Profile;
use App\Profile_online;
use App\Notification;


class MessageController extends Controller
{
    public function home()
    {
        return response()->json([ 'status'=>'Bad request', 'message'=>'Missing Parameters'],400);
    }

    // Store message data 
    public function store(Request $request){
        $inputs = $request->all();
        $rules = [
                    'sender_id'=>'required',
                    'type' =>'required',
                    'room_id'=>'required',
                ];
        $validator = Validator::make( $inputs , $rules);
        if($validator->fails()){
            return response()->json([ 'status'=>'Bad request','message'=>'Missing Parameters'],400);
        }
        $message = new Messages;
        $message->sender_id = $inputs['sender_id'];
        $message->receiver_id = $inputs['receiver_id']==""?0:$inputs['receiver_id'];
        $message->room_id = $inputs['room_id'];
        $message->type  = $inputs['type'];
        if ($inputs['type'] == 'text') {
            $message->contents = $inputs['content'];
        }else{
            $url = $this->upload_file($request);
            if($url){
                $message->url = $url;
            }else{
                return response()->json([ 'status'=>'Upload fail','message'=>'Fail to upload file!'],400);
            }
        }
        if ( $message->save() ) {
            // Get members in rooms
            $members = Chat_paticipate::where('room_id','=',$message->room_id)
                                        ->where('prof_id','!=',$message->sender_id)
                                        ->get();
            // save last message 
            Chat_rooms::where('id',$message->room_id)->update(array('last_message' => json_encode($message) ) );
            if ($members) {
                $redis = Redis::connection();
                foreach ($members as $member ) {
                    // Check if members online or not
                    $onlines = Profile_online::where('prof_id','=' , $member->prof_id)
                                            ->where('status', '=' , 1 )
                                            ->get();
                    $message = Messages::where('id',$message->id)->first();

                    if (!empty($onlines->toArray())) {
                        // User online 
                        foreach ($onlines as $online) {
                            $redis->publish("messages", json_encode(array("status" => 200, "socket_id" => $online->socket_id, "message" => $message)));
                        }
                    }else{
                        $title = $request->user->first_name . " send you a message.";
                        $data = array(
                                "type" => "message",
                                "data" => array(
                                        "room_id" => $message->room_id
                                    )
                            );
                        $oneSignal_ids =  Notification::where('prof_id',$member->prof_id)->get();

                        if (!empty($oneSignal_ids)) {
                            foreach ($oneSignal_ids as $osi) {
                                OneSignal::sendNotificationToUser($title, $osi->nof_id, $url = null, $data = $data);      
                            }
                        }
                    }
                }
                return response()->json([ 'status'=>'success','message'=>'Message sent'],200);
            }
        }
        return response()->json([ 'status'=>'Error','message'=>'Something went wrong!'],401);
    }

    // New chat room 
    public function create_room(Request $request){
        $inputs = $request->all();
        $rules = [
                    'paticipate_ids' =>'required',
                    'type' =>'required',
                ];
        $validator = Validator::make( $inputs , $rules);
        if($validator->fails()){
            return response()->json([ 'status'=>'Bad request','message'=>'Missing Parameters'],400);
        }

        // Check if user and fri already has a room
        if ($inputs['type'] == 'private'){
            $user_id = $request->user->id;
            $fri_id = 0;
            foreach ($inputs['paticipate_ids'] as $id) {    
                if ($id != $user_id) {
                    $fri_id = $id;
                    break;
                }
            }
            $chat_pat = Chat_paticipate::select(array('room_id' , DB::raw('COUNT(chat_participants.room_id) as cr')))
                                    ->where(function($query) use ($user_id , $fri_id) {
                                        $query->orWhere('prof_id' , $user_id);
                                        $query->orWhere('prof_id', $fri_id);
                                    })
                                    ->groupBy('room_id')
                                    ->having('cr', '=', 2)->first();
            if ($chat_pat) {
                $room = Chat_rooms::where('id', $chat_pat->room_id)->first();
                $members = Profile::select('id','img','first_name','last_name')->whereIn('id',$inputs['paticipate_ids'])->get();
                $room->members = $members;
                unset($room->last_message);
                return response()->json($room,200); 
            }
        }

        // Create new room 
        $room = new Chat_rooms;
        $room->creator_id = $request->user->id;
        $room->type  = $inputs['type'];
        if ($room->save()) {
            foreach ($inputs['paticipate_ids'] as $row) {
                $this->add_member_to_room($room->id , $row);
            }
            $members = Profile::select('id','img','first_name','last_name')->whereIn('id',$inputs['paticipate_ids'])->get();
            $room->members = $members;
            return response()->json($room,200);   
        }
        return response()->json([ 'status'=>'Error','message'=>'Something went wrong.'],401);
    }

    // Remove chat room with room id
    public function remove_chat_room(Request $request , $room_id){
        $is_used_to_chat = Messages::where('room_id',$room_id)->first();
        if($is_used_to_chat){
            $del_room = Delete_chat_room::firstOrNew(array('prof_id'=>$request->user->id ,'room_id'=>$room_id));
            $del_room->deleted_at = Carbon::now();
            if ($del_room->save()) {
                return response()->json([ 'status'=>'success','message'=>'remove completed'],200);
            }
        }else{
            // Delete empty chat room
            $remove = Chat_rooms::where('id',$room_id)->delete();
            if ($remove) {
                $this->remove_member_by_key_value('room_id',$room_id);
                return response()->json([ 'status'=>'success','message'=>'remove completed'],200);
            }
        }
        return response()->json([ 'status'=>'Error','message'=>'Something went wrong.'],401);
    }

    // List chat room by current user
    public function get_chat_rooms(Request $request){
        // DB::enableQueryLog();
        // dd(DB::getQueryLog());
        $roomId = isset($request->room_id) ? $request->room_id : 0;
        $user_id = $request->user->id;
        $page = $request->page ? $request->page : 1;
        $limit = $request->limit ? $request->limit : 10;
        $roomlist = DB::table('chat_rooms')
                ->select('chat_rooms.*')
                ->join('chat_participants','chat_participants.room_id','=','chat_rooms.id')
                ->where('chat_participants.prof_id','=',$user_id);
                if ( $roomId ){
                	$roomlist->where('chat_rooms.id','=',$roomId);
        		}
         $rooms = $roomlist->where('chat_participants.status','=',"publish")
                ->skip(($page-1)*$limit)
                ->take($limit)
                ->orderBy('chat_rooms.updated_at','desc')
                ->get();
        // get room members
        $response = array();
        foreach ($rooms as $room) {
            $member = DB::table('chat_participants')
                        ->select('profiles.id','profiles.img','profiles.first_name','profiles.last_name')
                        ->join('profiles','profiles.id','=','chat_participants.prof_id')
                        ->where('chat_participants.room_id',$room->id)
                        // ->where('chat_participants.prof_id','!=',$user_id)
                        ->get();
            if ($member) {
                $room->members = $member;
            }
            $last_message = json_decode($room->last_message,true);
            $msg = Messages::where('id',$last_message['id'])->first();
            $room->last_message = $msg;

            // $user_id = $request->user->id;
            $dateDeleteRoom = '';
            $deleteChatRoom = Delete_chat_room::where('prof_id', $user_id)->where('room_id', $room->id)->orderBy('deleted_at', 'DESC')->first();
            if ( $deleteChatRoom != null ){
            	$dateDeleteRoom = $deleteChatRoom->deleted_at;
            }
            	
            $chatMessageList = Messages::where('sender_id','!=', $user_id)
            	->where('status', 0)
            	->where('room_id', $room->id);
            if ( $dateDeleteRoom ){
            	$chatMessageList->where('updated_at', '>', $dateDeleteRoom);
            }
            
            $room->unread = $chatMessageList->count();
            
//             $room->unread = $chatMessage;
            
            // Check if user used to delete chat or received new chat 
            $is_user_delete = Delete_chat_room::where('room_id',$room->id)->where('prof_id',$user_id)->first();
            if ($is_user_delete) {
                // $last_message = Messages::where('created_at','>',$is_user_delete->deleted_at)
                //                 ->orderBy('id','desc')
                //                 ->first();
                if ($msg){
                    if ($msg->created_at > $is_user_delete->deleted_at) {
                        $response[]  = $room;
                    }    
                }
            }else{
                $response[]  = $room;
            }
        }
        return response()->json($response,200);   
    }

    // Fetch message from chat room by room id
    public function get_chat_room_message(Request $request , $room_id){
        $user_id = $request->user->id;
        $inputs = $request->all();
        $page = $request->page ? $request->page : 1;
        $limit = $request->limit ? $request->limit : 20;
        $is_user_delete = Delete_chat_room::where('room_id',$room_id)->where('prof_id',$user_id)->first();
        $instant_msg = Messages::where('room_id',$room_id);
        if ($is_user_delete) {
            $instant_msg->where('created_at','>',$is_user_delete->deleted_at);
        }
        $messages = $instant_msg->skip(($page-1)*$limit)
                ->take($limit)
                ->orderBy('created_at','desc')
                ->get();
        $reverse = array_reverse($messages->toArray());
        return response()->json($reverse,200);
    }

    // Add more member to chat room  
    // request from client
    public function add_member_to_chat_room(Request $request){

    }

    // Add chat room member or participante
    private function add_member_to_room($room_id , $user_id){
        $member = new Chat_paticipate;
        $member->prof_id = $user_id;
        $member->room_id = $room_id;
        $member->status = 'publish';
        $member->join_date = Carbon::now();
        return $member->save();
    }

    // Remove chat paticipate 
    // key : room_id  , prof_id .....
    private function remove_member_by_key_value($key , $value){
        return Chat_paticipate::where($key,$value)->delete();
    }

    private function upload_file($request , $type='img'){
        if ($request->file('file')) {
            $file = $request->file('file');
            $path = public_path('uploads').'/message/';
            $file_name = 'file-'. date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$file->getClientOriginalExtension();
            if($file->move($path, $file_name)){
                return '/public/uploads/message/'.$file_name;
            }
        }
        return null;
    }
    
    /*
     * status = 0 unread
     * status = 1 read
     */
    public function messageStatus(Request $request){
    	$messageId = $request->message_id;
    	$roomId = $request->room_id;
    	$receiverId = $request->user->id; // receiver_id
    	if ($roomId){
    		Messages::where('room_id', $roomId)->where('receiver_id', $receiverId)->update(['status' => 1]);
    	}else{
	    	$message = Messages::find($messageId);
	    	if ($message != null){
	    		$message->status = 1;
	    		$message->save();
	    	}
    	}
    	
    	return response()->json([ 'status'=>'success','message'=>'updated'],200);
    }
    
    /*
     * status = 0 unread
     * status = 1 read
     */
    public function messageStatusLists( Request $request ){
    	// DB::enableQueryLog();
    	// dd(DB::getQueryLog());
    	$user_id = $request->user->id;
    	$page = $request->page ? $request->page : 1;
    	$limit = $request->limit ? $request->limit : 10;
    	$rooms = DB::table('chat_rooms')
    	->select('chat_rooms.*')
    	->join('chat_participants','chat_participants.room_id','=','chat_rooms.id')
    	->where('chat_participants.prof_id','=',$user_id)
    	->where('chat_participants.status','=',"publish")
    	->skip(($page-1)*$limit)
    	->take($limit)
    	->orderBy('chat_rooms.updated_at','desc')
    	->get();
    	// get room members
    	$response = array();
    	$unread = 0;
    	foreach ($rooms as $room) {
    		// $user_id = $request->user->id;
    		$dateDeleteRoom = '';
    		$deleteChatRoom = Delete_chat_room::where('prof_id', $user_id)->where('room_id', $room->id)->orderBy('deleted_at', 'DESC')->first();
    		if ( $deleteChatRoom != null ){
    			$dateDeleteRoom = $deleteChatRoom->deleted_at;
    		}
    		 
    		$chatMessageList = Messages::where('sender_id','!=', $user_id)
    		->where('status', 0)
    		->where('room_id', $room->id);
    		if ( $dateDeleteRoom ){
    			$chatMessageList->where('updated_at', '>', $dateDeleteRoom);
    		}    	
    		$unread += $chatMessageList->count();
    	
    	}
    	return response()->json(['unread' => $unread],200);
    }
}
