<?php namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Online;
use Illuminate\Support\Facades\Redis;
use Input;

class SocketController extends Controller {
    public function __construct()
    {
//        $this->middleware('guest');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token,  Authorization, Set-Sessions');
    }
    public function index()
    {
        return view('socket');
    }

    public function test()
    {
        return view('home');
    }

    public function writemessage()
    {
        //$redis = Redis::connection();
        //$data = ['message' => Request::input('message'), 'user' => Request::input('user')];
        //$redis->publish('messages', Request::input('message'));
        return view('writemessage');
    }

    public function sendMessage(){
        $redis = Redis::connection();
        $message = Input::get('message');
        $onlines = Online::get();
        echo count($onlines);
        foreach($onlines as $o){
            echo $o->socket_id."<br />";
            echo $message.'<br />';
//            $redis->publish("message", json_encode(array("status" => 200, "socket_id" => $o->socket_id, "msg" => $message)));
            $redis->publish("messages", json_encode(array("status" => 200, "socket_id" => $o->socket_id, "msg" => $message)));
        }

//        return redirect('writemessage');

//        $redis = LRedis::connection();
//        $data = ['message' => Request::input('message'), 'user' => Request::input('user')];
//        $redis->publish('message', json_encode($data));
//        return response()->json([]);
    }
}