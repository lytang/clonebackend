<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

use Validator;
use DB;
use App;
use Input;
use Hash;
use App\Models\UserMobile;
use App\Models\UserMobileList;
use App\Models\MobileListe;
use App\Models\MobileGroup;
use App\Models\UserToken;
use OneSignal;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()	{
//        OneSignal::sendNotificationToUser("Message", 'a1357030-8515-488c-bee5-3e20757fa10d', $url = null, $data = null, $buttons = null, $schedule = null);
	    $userMobile = UserMobile::orderBy('users_mobile.name')->get();
	    return view('admin.index', ['userMobile' => $userMobile]);		
	}
	
	public function getUserCreate( $id = null){
	    $userMobile = UserMobile::find($id);
	    return view('admin.user-create', ['userMobile' => $userMobile]);
	}
	
	public function postUserStore(Request $request)
    {

        $id = Input::get('id');
        $phone = Input::get('mobile');
        $types = Input::get('types', 'upload');
        $mobileList = null;
        $messages = array(
            'mobile.required' => 'សូមបញ្ចូលលេខទូរសព្ទ',
            'mobile.unique' => 'លេខទូរសព្ទមានរួចហើយនៅក្នុងប្រពន្ធ័',
        );

        if (!$id){
            $this->validate($request, [
                'mobile' => 'required|unique:users_mobile',
            ], $messages);
        }else {
            $this->validate($request, [
                'mobile'    => 'required|unique:users_mobile,mobile,'.$id,
            ], $messages);
        }

	    $name = trim(Input::get('name'));
	    
	    $userMobile = UserMobile::find($id);
	    if ( $userMobile == null ){
	        $userMobile = new UserMobile;
            $userMobile->mobile = $phone;
	    }

        $userMobile->name = $name;
        $userMobile->types = $types;
        $userMobile->save();

        return Redirect::route('admin.index');
	}

	public function getUserDelete( $id = 0 ){
        $userMobile = UserMobile::find($id);
        if ( $userMobile != null ){
            $userMobile->delete();
        }
        return Redirect::route('admin.index');
    }
}
