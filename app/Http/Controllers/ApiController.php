<?php namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
//use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;
use Mockery\Matcher\HasKey;
use Validator;
use DB;
use App;
use Input;
use Hash;

use Response;
use Keygen;

use Storage;

use Illuminate\Support\Facades\Redis;
use Jenssegers\Agent\Agent;

use Unisharp\FileApi\FileApi;
use App\Models\UserMobile;
use App\Models\GroupUserMobile;
use App\Models\Group;
use App\Models\Message;
use App\Models\Online;
use OneSignal;
use App\Models\Notifications;

//use App\Models\UserToken;
//use Illuminate\Support\Facades\Crypt;
//use NotificationChannels\OneSignal\OneSignalMessage;
//use NotificationChannels\OneSignal\OneSignalChannel;
//use NotificationChannels\OneSignal\OneSignalWebButton;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use Illuminate\Notifications\Notification;


class ApiController extends Controller {

    public $paginate = 15;

    public $tokenVerify = false;

    public $user = null;
	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){
// 		$this->middleware('auth:api');
        $this->user = $this->validatedToken();
        if ( $this->user != null ){
            $this->tokenVerify = true;
        }
	}


    private function validatedToken(){
        $token = Input::header('apitoken');
        return UserMobile::where('token', $token)
            ->first();
    }

	public function postLogin(){
        $agent = new Agent();
        $devicename = $agent->device();
        $platform = $agent->platform();
        $browser = $agent->browser();

        $mobile     = Input::header('mobile');
        $deviceId   = Input::header('deviceId');
//        echo $request->mobile;

        if ($agent->isRobot() || !$agent->isMobile() || !$agent->isTablet()){
// 	        return Response::json(['status' => 0, 'msg' => "Mobile or Tablet ONLY"]);
        }

        $user = UserMobile::where('mobile', $mobile)
//            ->where('logined', 0)
//            ->where('disabled', 0)
            ->first();

        if ( $user == null ){
            return Response::json(['status' => 0, 'msg' => "Please System ERROR !!!"]);
        }

        if ( $user->token == "" ){
            $user->token = $this->generateTokenKey( $user->id );
        }

        $user->logined = 1;
        $user->mobile_id = $deviceId;

        $user->device_id = $deviceId;

        if ( $user->save() ){
            return Response::json([
                'status' => 1,
                'msg' => "Successed",
                'token' => $user->token,
                'userId' => $user->id,
                'mobileID' => $user->mobile_id
            ]);
        }else{
            return Response::json(['status' => 0, 'msg' => "Please System ERROR!!"]);
        }

    }

    public function postVerifyLogin(){
        $token = Input::header('apitoken');

        $user = UserMobile::where('token', $token)
            ->first();

        if ( $user != null ){
            return Response::json(['status' => 1, 'msg' => "Successed"]);
        }else{
            return Response::json(['status' => 0, 'msg' => "Expired token"]);
        }
    }

    public function postGroups(){

        if ( !$this->tokenVerify ){
            return Response::json(['status' => 201, 'msg' => "Expired token"]);
        }

        $token = Input::header('apitoken');
        return Group::selectRaw(DB::Raw('groups.id, groups.name'))
            ->addSelect(DB::Raw('(select count(*) from notifications where user_id = users_mobile.id and group_id = groups.id and `read` = 0) as nbGroups'))
            ->join('groups_user_mobiles', 'groups_user_mobiles.group_id', '=', 'groups.id')
            ->join('users_mobile', 'users_mobile.id', '=', 'groups_user_mobiles.user_mobile_id')
            ->where('users_mobile.token', $token)
            ->orderBy('nbGroups', 'DESC')
            ->orderBy('groups.name')
            ->groupBy('groups.id')
            ->distinct()
            ->get();
    }


    public function deviceID(){
        if ( !$this->tokenVerify ){
            return Response::json(['status' => 201, 'msg' => "Expired token"]);
        }
        $token = Input::header('apitoken');
        $deviceId   = Input::header('deviceId');
        UserMobile::where('token', $token)->update(['mobile_id' => $deviceId]);

        return Response::json(['status' => 200, 'msg' => "Success"]);
    }

    public function messageStatus(){
        if ( !$this->tokenVerify ){
            return Response::json(['status' => 201, 'msg' => "Expired token"]);
        }

        $token = Input::header('apitoken');
        $groupId = Input::get('group_id');
        $memberId = Input::get('member_id');
        $meesages = Input::get('messages', []);

        if ( count($meesages) > 0 ){

//            DB::table();
            Notifications ::whereIn('message_id', $meesages)
                ->where('group_id', $groupId)
                ->where('user_id', $this->user->id)
                ->update(['read' => 1]);

            return Response::json(['status' => 200, 'msg' => "Success"]);
        }
        return Response::json(['status' => 201, 'msg' => "Error"]);

    }

    public function postMembers(){

        if ( !$this->tokenVerify ){
            return Response::json(['status' => 201, 'msg' => "Expired token"]);
        }

        $token = Input::header('apitoken');
        $groupId = Input::get('group_id');

        return Group::selectRaw(DB::Raw('user_num.name, user_num.id'))
            ->join('groups_user_mobiles', 'groups_user_mobiles.group_id', '=', 'groups.id')
            ->join('users_mobile', 'users_mobile.id', '=', 'groups_user_mobiles.user_mobile_id')
            ->join('user_num', 'user_num.id', '=', 'groups_user_mobiles.user_num_id')
            ->where('users_mobile.token', $token)
            ->where('groups_user_mobiles.group_id', $groupId)
            ->orderBy('user_num.name')
            ->get();
    }

    private function generateTokenKey( $userId = null ){
        $keygen = $userId . DATE('Y-m-d-His') . Keygen::numeric(20)->generate();
        return Hash::make($keygen);
    }

    private function getUserNumIDByUser( $userMobileId, $groupId ){
        return GroupUserMobile::where('user_mobile_id', $userMobileId)->where('group_id', $groupId)->first();
    }
    /*
     * @types
     * 0 : text by default
     * 1 : images
     * 2 : voice
     */
    public function postMessageSend(Request $request){
        if ( !$this->tokenVerify ){
//            return Response::json(['status' => 201, 'msg' => "Expired token"]);
        }

        $token = Input::header('apitoken');
        $apiUserId = Input::header('apiuser', 0);

        $groupId = Input::get('group_id');
        $userNumId = Input::get('member_id', 0);

        $messageSent = Input::get('message');
        $types = Input::get('types', 0);
        $duration = Input::get('duration', 0);

        $message = new Message;
        $message->group_id = $groupId;
        if ( $userNumId == 0 ){
            $userNum = null;
            if ( $this->user->types == 'upload' ){
                $userNum = $this->getUserNumIDByUser($apiUserId, $groupId);
            }
            $message->user_num_id = $userNum != null ? $userNum->user_num_id : 0;
        }else{
            $message->user_num_id = $userNumId;
        }

        if ( $types == 0 ){
            $message->message = $messageSent;
        }elseif ( $types == 1 ){
            $filename = $this->imageUploadPost($request);
            $message->message = $filename;
        }elseif ( $types == 2 ){
            $filename = $this->imageUploadPostVideo($request);
            $message->message = $filename;
            $message->duration = $duration;
        }

        $message->user_id = $apiUserId;
        $message->cdate = DATE('Y-m-d H:i:s');
        $message->types = $types;
        if ( $message->save() ){
            $redis = Redis::connection();
            $memberBroadcase = $this->memberGroup($groupId, $token, $userNumId);

//            $onlines = Online::where('status', 1)->get();
//            foreach($onlines as $o){
//                $redis->publish("messages", json_encode(array("status" => 200, "socket_id" => $o->socket_id, "message" => $message)));
//            }

            $groupob = Group::find($groupId);
            foreach($memberBroadcase as $userSocket){
                $socketOnline = $this->realOnlineUser($userSocket->id);
                if ( $socketOnline != null ){
                    $redis->publish("messages", json_encode(array("status" => 200, "socket_id" => $socketOnline->socket_id, "message" => $message)));
                }else{
                    if ( trim($userSocket->mobile_id) != "" ){
                        // push notification
                    //    echo "Push = ".$userSocket->mobile_id."<br />";
                        // if ( $user->token != $token ){
                            OneSignal::sendNotificationToUser("Message", $userSocket->mobile_id, $url = null, $groupob, $buttons = null, $schedule = null);
                        // }

                    }
                }
            }

            return Response::json(['status' => 1, 'msg' => "Successed", 'messeage' => $message]);
        }else{
            return Response::json(['status' => 0, 'msg' => "Error"]);
        }
    }

    private function memberGroup( $groupId, $token = 0, $userNumId = 0 ){
        $groupUserMobile = GroupUserMobile::select('users_mobile.id', 'users_mobile.mobile_id')
            ->join('users_mobile', 'users_mobile.id', '=', 'groups_user_mobiles.user_mobile_id')
//            ->leftJoin('onlines', 'onlines.user_id', '=', 'groups_user_mobiles.user_mobile_id')
            ->where('groups_user_mobiles.group_id', $groupId);
            //->where('users_mobile.token', $token);
        if ( $userNumId ){
            $groupUserMobile->where('user_num_id', $userNumId);
        }
        return $groupUserMobile
            ->distinct()
            ->get();
    }

    private function realOnlineUser( $userId ){
        return Online::where('user_id', $userId)->where('status', 1)->first();
    }

    private function imageUploadPostVideo($request){
//        $this->validate($request, [
//            'apiimage' => 'required|mimes:mp4,avi,asf,mov,qt,avchd,flv,swf,mpg,mpeg,mpeg-4,wmv,divx,3gp,aac|max:20480',
//        ]);

        $imageName = time().'.'.$request->apiimage->getClientOriginalExtension();

        $imageName = Md5($this->tokenVerify .$imageName).'.'.$request->apiimage->getClientOriginalExtension();

        $image = $request->file('apiimage');
        $t = Storage::disk('s3')->put($imageName, file_get_contents($image), 'public');
        $imageName = Storage::disk('s3')->url($imageName);

        return $imageName;
    }

    private function imageUploadPost($request)
    {
        $this->validate($request, [
            'apiimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20480',
        ]);

        $imageName = time().'.'.$request->apiimage->getClientOriginalExtension();

        $imageName = Md5($this->tokenVerify .$imageName).'.'.$request->apiimage->getClientOriginalExtension();

        $image = $request->file('apiimage');
        $t = Storage::disk('s3')->put($imageName, file_get_contents($image), 'public');
        $imageName = Storage::disk('s3')->url($imageName);

        return $imageName;
    }

    public function postMessageSendBackup(){
        if ( !$this->tokenVerify ){
            return Response::json(['status' => 201, 'msg' => "Expired token"]);
        }

        $token = Input::header('apitoken');
        $groupId = Input::get('group_id');
        $memberId = Input::get('member_id');

        $messageSent = Input::get('message');
        $types = Input::get('types', 0);

        $message = new Message;
        $message->group_id = $groupId;
        $message->user_num_id = $memberId;
        if ( $types == 0 ){
            $message->message = $messageSent;
        }elseif ( $types > 0 ){
            $fa = new FileApi('/images/'); # use default path (as '/images/')

            $filename = Md5($token.DATE('YmdHis'));

            $file = $fa->save(Input::file('apiimage'), $filename); // => custom-file-name.jpg
            $message->message = $file;
        }

        $message->cdate = DATE('Y-m-d');
        $message->types = $types;
        if ( $message->save() ){
            return Response::json(['status' => 1, 'msg' => "Successed", 'messeage' => $message]);
        }else{
            return Response::json(['status' => 0, 'msg' => "Error"]);
        }
    }

    public function postMessagesList(){

	    if ( !$this->tokenVerify ){
            return Response::json(['status' => 201, 'msg' => "Expired token"]);
        }

        $token = Input::header('apitoken');
        $groupId = Input::get('group_id');
        $memberId = Input::get('member_id', 0);
        $count = Input::get('count', 0);

        $messages = Message::select('messages.*')
            ->addSelect(DB::Raw('(case when notifications.read is null then 0 else notifications.read end) as `read`'))
            ->leftJoin('notifications', 'notifications.message_id', '=', 'messages.id')
            ->join('groups_user_mobiles', 'groups_user_mobiles.group_id', '=', 'messages.group_id')
            ->join('users_mobile', 'users_mobile.id', '=', 'groups_user_mobiles.user_mobile_id')
            ->whereNotNull('messages.message');
            if ( $this->user->types == 'upload' ){
                //$messages->join('user_num', 'user_num.id', '=', 'groups_user_mobiles.user_num_id');
                $messages->whereRaw('(groups_user_mobiles.user_num_id = messages.user_num_id OR messages.user_num_id = 0)');
            }

        $messages->where('users_mobile.token', $token)
            ->where('messages.group_id', $groupId)
            ->whereRaw('messages.group_id = groups_user_mobiles.group_id');

        if ( $memberId ){
            $messages->where('messages.user_num_id', $memberId);
        }
        return $messages
            ->orderBy('messages.cdate', 'DESC')
            //->orderBy('read', 'DESC')
            ->groupBy('messages.id')
            ->distinct()
            ->offset($count)
            ->limit($this->paginate)
            ->get();
    }

    public function postMessageDelete(){
        if ( !$this->tokenVerify ){
            return Response::json(['status' => 201, 'msg' => "Expired token"]);
        }

        $messageId = Input::get('message_id');
        Message::find($messageId)->delete();

        return Response::json(['status' => 1, 'msg' => "Successed"]);
    }

    public function onlineUser( ){
        $socketId = Input::get('socket_id');
        $status = Input::get('status');
        $userId = Input::get('user_id');
        Online::where('user_id', $userId)->update(['status' => 0]);

        $online = Online::where('socket_id', $socketId)->first();
        if ( $online == null ){
            $online = new Online;
            $online->socket_id = $socketId;
            $online->user_id = $userId;
        }
        $online->status = $status;

        if ($online->save()) {
            return Response::json([ 'status'=>'success','message'=>'success'],200);
        }
        return Response::json(['status'=>'Error', 'message'=>"Something error"],401);
    }
}
