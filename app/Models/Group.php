<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use App\Models\UserGroup;
use DB;

class Group extends Model
{
    protected $table = 'groups';

    public $timestamps = false;

}
