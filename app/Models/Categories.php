<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use App\Models\UserGroup;
use DB;

class Categories extends Model
{
    protected $table = 'categories';

    public $timestamps = false;

    public function cat_group(){
    	return $this->hasMany('App\Models\UserGroup', 'group_id', 'id');
    }
    
    public function getListGroup( $userId )
    {
    	return Categories::select('categories.title', 'categories.id')
    		->join('user_groups', 'user_groups.group_id', '=', 'categories.id')->where('user_groups.user_id', $userId)->get();
    }
    
}
