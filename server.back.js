var express =   require('express'),
    http =      require('http'),
    server =    http.createServer(app);

var app = express();

var querystring = require('querystring');
// var https = require('https');
var https = require('http');
var host = 'bchetthor.thadatra.com';//spade.demo-kd.com

const redis =   require('redis');
const io =      require('socket.io');

const redisClient = redis.createClient();
redisClient.subscribe('messages');

var clients = [];
server.listen(3000);
console.log("Listening.....");


io.listen(server).on('connection', function(socket) {

    clients[socket.id] = socket;

    // console.log(Object.size(clients));
    // God the same ip address.
    // var address = socketClient.handshake.address;
    // var clientIp = socketClient.request.connection.remoteAddress;
    var ip = socket.request.headers['x-forwarded-for'] || socket.request.connection.remoteAddress;

    // socket listener
    // listen when socket is dissconnet from server
    socket.on('disconnect', function() {
        var index = 0;
        var foundIndex = -1;
        for(var key in clients){
            if (key == socket.id){
                foundIndex = index;
                break;
            }
            index++;
        }
        console.log("index : " + foundIndex);
        if (foundIndex > -1 ) {
            clients.splice(foundIndex, 1);
        }
        var data = {
            "socket_id" : socket.id,
            "user_id" : socket.user_id,
            "ip" : ip,
            "status" : 0,
        };
        console.log("remove user "+socket.user_id);
        performRequest("/api/online-user","POST",data,function(response){
            // on success callback
        });
    });

    // subscribe room for client
    socket.on("subscribe",function(room){
        socket.join(room);
    });

    // when user connected to the socket , must update user data
    socket.on('adduser', function(user_id){
        socket.user_id = user_id;
        console.log("add user "+user_id);
        var data = {
            "socket_id" : socket.id,
            "user_id" : user_id,
            "ip" : ip,
            "status" : 1,
        };
        performRequest("/api/online-user","POST",data,function(response){
            // on success callback
        });
    });
});


redisClient.on("message", function(channel, message) {
    var messages = JSON.parse(message);
    console.log("send message  to socket : "+messages['message']['contents']);
    var msocket = clients[messages['socket_id']];
    if (msocket) {
        msocket.emit(channel , messages['message']);
    }
});

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function performRequest (endpoint, method, data, success , fail) {
    var dataString = querystring.stringify(data);
    var headers = {};

    if (method == 'GET') {
        endpoint += '?' + querystring.stringify(data);
    }
    else {
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',//multipart/form-data
            'Content-Length': Buffer.byteLength(dataString)
        };
    }
    var options = {
        host: host,
        port:8000,
        path: endpoint,
        method: method,
        headers: headers,
    };

    var req = https.request(options, function(res) {
        res.setEncoding('utf-8');

        if(res.statusCode == 200){
            var responseString = '';
            res.on('data', function(data) {
                responseString += data;
            });

            res.on('end', function() {
                // console.log(responseString);
                var responseObject = JSON.parse(responseString);
                success(responseObject);
            });
        }else{
            if (fail) {
                fail();
            }
        }
    }).on('error',function () {
        console.log(e.message);
    });
        //
        // console.log('Got error: ${e.message}');

    req.write(dataString);
    req.end();
}