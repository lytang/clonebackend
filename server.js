var express =   require('express'),
    http =      require('http'),
    server =    http.createServer(app);

var app = express();

var querystring = require('querystring');
// var https = require('https');
var https = require('http');
var host = 'bchetthor.thadatra.com';//spade.demo-kd.com

const redis =   require('redis');
// const io =      require('socket.io');
const io = require('socket.io')(server);

const redisClient = redis.createClient();
redisClient.subscribe('messages');

var clients = [];
var testing = "";
server.listen(3000);
console.log("Listening.....");

io.listen(server).on('connection', function(socket) {

    clients[socket.id] = socket;
    var ip = socket.request.headers['x-forwarded-for'] || socket.request.connection.remoteAddress;
    // subscribe room for client
    socket.on("subscribe",function(room){
        socket.join(room);
        console.log("sub "+room);
    });

    // listen when socket is dissconnet from server
    socket.on('disconnect', function() {
        var index = 0;
        var foundIndex = -1;
        for(var key in clients){
            if (key == socket.id){
                foundIndex = index;
                break;
            }
            index++;
        }
        console.log("index : " + foundIndex);
        if (foundIndex > -1 ) {
            clients.splice(foundIndex, 1);
        }
        var data = {
            "socket_id" : socket.id,
            "status" : 0,
        };
        console.log("User : "+socket.user_id+" => Socket ID : "+socket.id+" OFFLINE ");
        performRequest("/online-user","POST",data,function(response){
            // on success callback
        });
    });

    // when user connected to the socket , must update user data
    socket.on('adduser', function(user_id){
        socket.user_id = user_id;
        console.log("User : "+user_id.user_id+" => Socket ID : "+socket.id+" ONLINE ");

        var data = {
            "socket_id" : socket.id,
            "user_id" : user_id.user_id,
            "status" : 1,
        };
        performRequest("/online-user", "POST", data, function(response){
            // on success callback
            // console.log("adding "+response.message);
        });
    });

});

redisClient.on("message", function(channel, message) {
    var messages = JSON.parse(message);
    console.log("send message : "+ messages.message.message );
    var msocket = clients[messages.socket_id];
    if (msocket) {
        msocket.emit(channel , messages.message);
    }
});

function performRequest (endpoint, method, data, success , fail) {
    // console.log("performRequest");
    var dataString = querystring.stringify(data);
    var headers = {};

    if (method == 'GET') {
        endpoint += '?' + querystring.stringify(data);
    } else {
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',//multipart/form-data
            'Content-Length': Buffer.byteLength(dataString),
            'Access-Control-Allow-Origin': 'http://bchetthor.thadatra.com:3000',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
            'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
            'Access-Control-Allow-Credentials': true,
        };
    }
    // var options = {
    //     host: 'http://bchetthor.thadatra.com/online-user',
    //     port:8000,
    //     path: endpoint,
    //     method: method,
    //     headers: headers,
    // };


    var FormData = require('form-data');

    var form = new FormData();

    // var options = {
    //     url: 'http://bchetthor.thadatra.com/api/online-user',
    //     method: 'POST',
    //     headers: headers,
    //     form: {'socket_id': data.socket_id, 'user_id': 'yyy', 'status' : 1}
    // }

    // var req = http.request({
    //     method: 'post',
    //     host: 'http://bchetthor.thadatra.com',
    //     path: '/online-user',
    //     headers: form.getHeaders()
    // });

    form.append('socket_id', data.socket_id);
    form.append('user_id', data.user_id);
    form.append('status', data.status);

    form.submit('http://bchetthor.thadatra.com/api/online-user', function(err, res) {
        // res – response object (http.IncomingMessage)  //
        console.log("Code "+res.statusCode);
        res.resume();

    });

    // var req = https.request(options, function(res) {
    //     res.setEncoding('utf-8');
    //
    //     console.log("request "+res.statusCode);
    //
    //     if(res.statusCode == 200){
    //         console.log("200");
    //         var responseString = '';
    //         res.on('data', function(data) {
    //             responseString += data;
    //         });
    //
    //         res.on('end', function() {
    //             // console.log(responseString);
    //             var responseObject = JSON.parse(responseString);
    //             success(responseObject);
    //         });
    //     }else{
    //         if (fail) {
    //             fail();
    //         }
    //     }
    // }).on('error',function (e) {
    //     console.log(e.message);
    // });
    //
    // console.log('Got error: ${e.message}');

    // req.write(dataString);
    // req.end();
}