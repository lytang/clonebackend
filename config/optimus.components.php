<?php

return [
    'namespaces' => [
        'Api' => base_path() . DIRECTORY_SEPARATOR . 'api',
        'Infrastructure' => base_path() . DIRECTORY_SEPARATOR . 'infrastructure'
    ],
    
    
    'protection_middleware' => [
        'auth:api' // <--- Checks for access token and logging in the user
    ],
    
    'resource_namespace' => 'resources',
    
    'language_folder_name' => 'lang',
    
    'view_folder_name' => 'views'
];

?>