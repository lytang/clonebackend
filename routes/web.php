<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('login', array('as' => 'login', 'uses' => 'HomeController@login'));
Route::get('logout', array('as' => 'logout', 'uses' => 'HomeController@logout'));
    
// Route::get('/', array('as' => 'login', 'uses' => 'HomeController@login'));
Route::get('/', function () {
    return view('welcome');
});
    
    
Route::post('authenticate', ['uses' => 'Auth\AuthController@authenticate', 'as' => 'authent']);
    
// Route::get('login', array('as' => 'authenticate', 'uses' => 'Auth\AuthController@authenticate'));

// Route::controllers([
//     'auth' => 'Auth\AuthController',
//     'password' => 'Auth\PasswordController',
// ]);
    
// Route::get('flush', function () {
//     session()->flush();
// });

Route::get('admin', array('as' => 'admin.index', 'uses' => 'AdminController@getIndex'));
Route::get('admin/user-create', array('as' => 'admin.user.create', 'uses' => 'AdminController@getUserCreate'));
Route::get('admin/user-edit/{id}', array('as' => 'admin.user.edit', 'uses' => 'AdminController@getUserCreate'));
Route::get('admin/user-delete/{id}', array('as' => 'admin.user.delete', 'uses' => 'AdminController@getUserDelete'));

Route::post('admin/user-store', array('as' => 'admin.user.store', 'uses' => 'AdminController@postUserStore'));


// Setting
Route::get('setting/group', array('as' => 'setting.group', 'uses' => 'SettingController@getGroup'));
Route::post('setting/group-load', array('as' => 'setting.group.load', 'uses' => 'SettingController@postLoadGroup'));
Route::post('setting/group-store', array('as' => 'setting.group.store', 'uses' => 'SettingController@postStoreGroup'));


Route::get('setting/group-user', array('as' => 'setting.group.user', 'uses' => 'SettingController@getGroupUser'));

Route::get('setting/setup-group', array('as' => 'setting.setup.group', 'uses' => 'SettingController@getSetupGroup'));
Route::post('setting/setup-group-load', array('as' => 'setting.setup.group.load', 'uses' => 'SettingController@setupGroupLoad'));
Route::post('setting/setup-group-store', array('as' => 'setting.setup.group.store', 'uses' => 'SettingController@setupGroupStore'));

// Route::controller('admin', 'AdminController', [
//     'getIndex' 				=> 'admin.index',
// ]);

Route::group(['prefix' => 'api'], function() {
    //all other api routes goes here    
    Route::post('login', array('as' => 'api.login', 'uses' => 'ApiController@postLogin'));
    Route::post('device-update', array('as' => 'api.device', 'uses' => 'ApiController@deviceID'));
    Route::post('login-verify', array('as' => 'api.login.verify', 'uses' => 'ApiController@postVerifyLogin'));
    Route::post('groups', array('as' => 'api.groups', 'uses' => 'ApiController@postGroups'));
    Route::post('members', array('as' => 'api.members', 'uses' => 'ApiController@postMembers'));
    Route::post('messages-list', array('as' => 'api.messages.list', 'uses' => 'ApiController@postMessagesList'));
    Route::post('message', array('as' => 'api.messages.send', 'uses' => 'ApiController@postMessageSend'));
    Route::post('message-delete', array('as' => 'api.messages.delete', 'uses' => 'ApiController@postMessageDelete'));
    Route::post('online-user', array('as' => 'api.online.user', 'uses' => 'ApiController@onlineUser'));
    Route::post('message-status', array('as' => 'api.message.status', 'uses' => 'ApiController@messageStatus'));

//    Route::get('message-test','MessageController@home');
//    Route::post('message-test','Api\MessageController@store');
//    Route::post('message/rooms','Api\MessageController@create_room');
//    Route::get('message/rooms','Api\MessageController@get_chat_rooms');
//    Route::get('message/rooms/{id}','Api\MessageController@get_chat_room_message')->where('id', '[0-9]+');
//    Route::delete('message/rooms/{id}','Api\MessageController@remove_chat_room')->where('id', '[0-9]+');
//    Route::get('message/liststatus','Api\MessageController@messageStatusLists');
//    Route::post('message/status','Api\MessageController@messageStatus');

});

Route::get('/images/event/{filename}', function ($filename) {
    $entry = new \Unisharp\FileApi\FileApi('/images/');
    return $entry->getResponse($filename);
});


Route::get('/avatars/{filename}', function ($filename)
{
    $path = storage_path() . '/app/images/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
})->name('avatar');

//https://s3-ap-southeast-1.amazonaws.com/line-clone/1520752915.png

Route::get('s3-image-upload', 'S3ImageController@imageUpload');
Route::post('s3-image-upload', 'S3ImageController@imageUploadPost');

//Route::get('/images/article/{filename}', function ($filename) {
//    $entry = new \Unisharp\FileApi\FileApi('/images/article/');
//    return $entry->getResponse($filename);
//});


Route::get('chat-test', 'SocketController@test');

Route::get('socket', 'SocketController@index');
Route::post('sendmessage', array('as' => 'sendMessage', 'uses' => 'SocketController@sendMessage'));
Route::get('writemessage', 'SocketController@writemessage');

//Route::get('online-user','ApiController@onlineUser');

Route::get('test', 'SocketController@test');






