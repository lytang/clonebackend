<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="{{ asset('js/socket.io-2.1.0.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.12.15/vue.min.js"></script>

<script>
    $(document).ready(function(){

        var socket  = new io.Socket(),
            text        = $('#text');

        socket.connect();

        socket.on('connect', function () {
            text.html('connected');
        });

        socket.on('message', function (msg) {
            text.html(msg);
        });

        socket.on('disconnect', function () {
            text.html('disconnected');
        });

    });
</script>


<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2" >
            <div id="text" ></div>
        </div>
    </div>
</div>