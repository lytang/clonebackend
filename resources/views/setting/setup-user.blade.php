@extends('layout.master')

@section('content')                                              
    <!-- Content -->
	<section id="content" class="container">
	    <!-- Breadcrumb -->
       	<ol class="breadcrumb hidden-xs">
			<li><a href="#">Home</a></li>
          	<li><a href="#">Library</a></li>
          	<li class="active">Data</li>
		</ol>
		<h4 class="page-title">Setup User</h4>
		<div class="listview list-container">
			<form method="post" id="setupUserForm">						
    				<div class="block-area" id="custom-select">
    					<p>User</p>
                    	<div class="row">
                        <div class="col-md-2 m-b-15">
                            <select class="select userList" name="user_mobile_id">
                            		<option value="0">Select</option>
                            		@if ( count($userMobile) > 0 )
                            			@foreach($userMobile as $um)
                            				<option value="{{ $um->id }}">{{ $um->username }}</option>
                            			@endforeach
                            		@endif
                            </select>
                        </div>
                                            
                        <div class="clearfix"></div>
                        @if ( count($listes) > 0 )
                    			@foreach($listes as $l)
                    				<div class="checkbox m-b-5">
                                    <label class="listeSelect">
                                        <input type="checkbox" name="liste_id[{{ $l->id }}]" value="{{ $l->id }}" />
                                        <span style="font-size: 16px;font-weight: bold;">{{ $l->title }}</span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                                
                                @if (isset($userMobileList[$l->id]))
                                		<?php $count = 0;?>
                                		@foreach($userMobileList[$l->id] as $uml)                            			                            			
                                        <label class="checkbox-inline listeChild{{ $l->id }}" style="width: 150px;padding:0px;margin: 0px;">
                                            <?php /*?><input type="checkbox" name="group_id[{{ $l->id }}][{{ $uml->id }}]" value="{{ $uml->id }}"><?php */?>
                                            <div>{{ $uml->name }}</div>
                                            <div>[<input type="checkbox" name="group_id[{{ $l->id }}][{{ $uml->id }}][kon]" value="{{ $uml->id }}" /> Kon]</div>
                                            <div>[<input type="checkbox" name="group_id[{{ $l->id }}][{{ $uml->id }}][kons]" value="{{ $uml->id }}" /> Kons]</div>
                                        </label>                  
                                        <?php $count++?>
                                        @if ( $count == 8 )
                                        		<?php $count = 0;?>
                                        		<div class="clearfix"></div><br />
                                        @endif           
                                		@endforeach
                                @endif
                                <hr class="whiter m-t-20" />
                    			@endforeach
                    		@endif
                    		<div class="clearfix"></div>
                    		{{ csrf_field() }}
                        <button type="button" class="btn btn-sm m-t-10 btnSave">Save</button>
                        	<button type="button" class="btn btn-sm m-t-10">Cancel</button>
                  	</div>
    				</div>
			</form>
		</div>
	</section>                
@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        loadSetupUser();
        $('select[name="user_mobile_id"]').on('change', function (){            
        		loadSetupUser();	
        });
        /* Tag Select */
        (function(){
            /* Limited */
            $(".tag-select-limited").chosen({
                max_selected_options: 5
            });
            
            /* Overflow */
            $('.overflow').niceScroll();
        })();   

    		// select checked of the liste
        loadSelectListe();
        
        $('.listeSelect').on('ifChecked', function(){
        		var listeId = $(this).find('input[type="checkbox"]').val();        		
        		$('.listeChild'+listeId+' input[type="checkbox"]').attr('disabled', false)
      	}); 

        $('.listeSelect').on('ifUnchecked', function(){
        		var listeId = $(this).find('input[type="checkbox"]').val();
        		$('.listeChild'+listeId+' input[type="checkbox"]').attr('disabled', true)
        });

        	$('button.btnSave').on('click', function (){
        		setupUserForm();
      	});
                   
    });

    function loadSelectListe(){
        $('.listeSelect input[type="checkbox"]').each(function (){
            var listeId = $(this).val();
            if ( $(this).is(':checked') ){
                $('.listeChild'+listeId+' input[type="checkbox"]').attr('disabled', false)
            }else{
                $('.listeChild'+listeId+' input[type="checkbox"]').attr('disabled', true)
            }
        });
    }
    
    function setupUserForm(){
        var user_mobile_id = $('select[name="user_mobile_id"]').val();
        if ( user_mobile_id == 0 ){
            alert("Please select User");
            return false;
        }

        var serirForm = $("#setupUserForm").serialize();
        $.ajax({
            type: "POST",
            url: "{{ route('setting.setup.user.store') }}",
            async: true,
            //dataType: "json",
            data: serirForm,
            success: function(data) {
                alert('OK');
            }
        });
    }
    
    function loadSetupUser(){
        
        var user_mobile_id = $('select[name="user_mobile_id"]').val();
        var token 		= "{!! csrf_token() !!}";

        $('.listeSelect input[type="checkbox"]').each(function (){
			$(this).iCheck('uncheck');
			var listeId = $(this).val();
			$('.listeChild'+listeId+' input[type="checkbox"]').iCheck('uncheck');		   			
        	}); 
    	
        
    		$.ajax({
                type: "POST",
                url: "{{ route('setting.setup.user.load') }}",
                async: true,
                //dataType: "json",
                data: {_token: token, user_mobile_id : user_mobile_id} ,
                success: function(data) {
                    if ( data['listes'].length > 0 ){
                        for( var i=0;i<data['listes'].length;i++ ){
                    		$('.listeSelect input[type="checkbox"][value='+data['listes'][i].liste_id+']').iCheck('check');
                        }
                    }
                    if ( data['groups'].length > 0 ){
                        for( var j=0;j<data['groups'].length;j++ ){
                            if ( data['groups'][j].kon == 1){
                    			$('.listeChild'+data['groups'][j].liste_id+' input[type="checkbox"][name="group_id['+data['groups'][j].liste_id+']['+data['groups'][j].group_id+'][kon]"]').iCheck('check');
                            }
                        
                            if ( data['groups'][j].kons == 1){
                    			$('.listeChild'+data['groups'][j].liste_id+' input[type="checkbox"][name="group_id['+data['groups'][j].liste_id+']['+data['groups'][j].group_id+'][kons]"]').iCheck('check');
                            }
                        }
                    }
                    loadSelectListe();
                }
        });
        
   	}
</script>
@stop