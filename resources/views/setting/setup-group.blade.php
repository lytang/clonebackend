@extends('layout.master')

@section('content')                                              
    <!-- Content -->
	<section id="content" class="container">
	   
            
		<h4 class="page-title">Setup Group</h4>
		<div class="listview list-container">
			<form method="post" id="setupGroupForm">						
    				<div class="block-area" id="custom-select">
    					<p>User</p>
                    	<div class="row">
                        <div class="col-md-2 m-b-15">
                            <select class="select groupList" name="group_id">
                            		<option value="0">Select</option>
                            		@if ( count($groups) > 0 )
                            			@foreach($groups as $g)
                            				<option value="{{ $g->id }}">{{ $g->title }}</option>
                            			@endforeach
                            		@endif
                            </select>
                        </div>
                                            
                        <div class="clearfix"></div>
                        <div id="loadData"></div>
                       	{{ csrf_field() }}
                       	                       	
                        <button type="button" class="btn btn-sm m-t-10 btnSave">Save</button>
                        	<button type="button" class="btn btn-sm m-t-10">Cancel</button>
                        	<button type="button" class="btn btn-sm m-t-10 btnGetImage">Get Image</button>
                  	</div>
    				</div>
			</form>
		</div>
	</section>                
@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
    		
        /* Tag Select */
        (function(){
            /* Limited */
            $(".tag-select-limited").chosen({
                max_selected_options: 5
            });
            
            /* Overflow */
            $('.overflow').niceScroll();
        })();   

        setupGroupLoad();
        $('select[name="group_id"]').on('change', function (){            
        		setupGroupLoad();	
        	});

        $('button.btnSave').on('click', function (){
        		setupGroupForm();
      	});

        $('button.btnGetImage').on('click', function (){
            	$('#loadData input[type="checkbox"]').each(function (){
            		if ( $(this).is(':checked') ){
                		alert($(this).val() + '_'+ $(this).attr('val'))
            		}
            	}); 
            	$.ajax({
        		  	url: 'http://images.thadatra.com/api/v1/screenshots',
        		  	method: 'POST',
        		  	data: {
        		    		screenshot: {
        		      		url: 'http://chetlaor.com/result/user-print-show/01112017/01112017/1-6-1019-0-0-0-1-16-0'
        		    		}
        		  	},
        		  	success: function(data) {
                  	alert(data);
               	}
        		})
      	});
        
    });

    function loadSelectListe(){
        	$('.listeSelect input[type="checkbox"]').each(function (){
    			var listeId = $(this).val();        
        		if ( $(this).is(':checked') ){
        			$('.listeChild'+listeId+' input[type="checkbox"]').attr('disabled', false)
        		}else{
        			$('.listeChild'+listeId+' input[type="checkbox"]').attr('disabled', true)
        		}
        	});    	
    }
    
    function setupGroupForm(){
    		var group_id = $('select[name="group_id"]').val();
    		if ( group_id == 0 ){
        		alert("Please select Group");
			return false;
        	}

    		var serirForm = $("#setupGroupForm").serialize();            
    		$.ajax({
            type: "POST",
            url: "{{ route('setting.setup.group.store') }}",
            async: true,
            //dataType: "json",
            data: serirForm,
            success: function(data) {
                alert('OK');
            }
        });
    }
    
    function setupGroupLoad(){
    		var token 		= "{!! csrf_token() !!}";
    		var group_id = $('select[name="group_id"]').val();
    	
    		$.ajax({
            type: "POST",
            url: "{{ route('setting.setup.group.load') }}",
            async: true,
            //dataType: "json",
            data: {_token: token, group_id : group_id} ,
            success: function(data) {            		
                $('#loadData').html(data.htmls);
                reloadCheckRadioBox();                
            }
        });
        
   	}
</script>
@stop