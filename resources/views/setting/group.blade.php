@extends('layout.master')

@section('content')                                              
    <!-- Content -->
	<section id="content" class="container">
	    <!-- Breadcrumb -->
       	<ol class="breadcrumb hidden-xs">
			<li><a href="#">Home</a></li>
          	<li><a href="#">Library</a></li>
          	<li class="active">Data</li>
		</ol>
		<h4 class="page-title">Setup User</h4>
		<div class="listview list-container">
			<form method="post" id="setupUserForm">						
                <div class="block-area" id="custom-select">
                    <div class="row">
                        <div class="col-md-2 m-b-15">
                            <p>User</p>
                            <select class="select userList" name="user_mobile_id">
                                <option value="0">Select</option>
                                @if ( count($userMobile) > 0 )
                                @foreach($userMobile as $um)
                                    <option usertype="{{ $um->types }}" value="{{ $um->id }}">{{ $um->name.' ['.$um->mobile.']' }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="groupList col-md-2 m-b-15">
                            <p>Groups</p>
                            <select class="select groupList" name="group_id">
                                <option value="0">Select</option>
                                @if ( count($groups) > 0 )
                                    @foreach($groups as $g)
                                        <option value="{{ $g->id }}">{{ $g->name }}</option>
                                    @endforeach
                                @endif
                            </select>

                        </div>

                        <div class="col-md-2 m-b-15">
                            <p>&nbsp;</p>
                            <button type="button" class="btn btn-sm btnSave">Save</button>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div id="uploaduser">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="100" class=""><input class="allchecbox" style="opacity: unset" type="checkbox" /></th>
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody id="loadData"></tbody>
                            </table>
                        </div>
                        <div id="loadDataGroup">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="100" class=""><input class="allchecbox" style="opacity: unset" type="checkbox" /></th>
                                    <th>ក្រុម</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ( count($groups) > 0 )
                                    @foreach($groups as $g)
                                    <tr>
                                        <td><input {{ (isset($gulsed[$g->id]) ? "checked=checked" : '') }} class="checkbox" style="opacity: unset;" type="checkbox" name="userGroup[{{$g->id}}]" value="{{ $g->id }}" /></td>
                                        <td>{{ $g->name }}</td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-sm m-t-10 btnSave">Save</button>
                        <button type="button" class="btn btn-sm m-t-10">Cancel</button>
                    </div>
                </div>
			</form>
		</div>
	</section>
    <br /><br /><br /><br />
@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        var user_mobile = $('select[name="user_mobile_id"]');
        var usertype = $('option:selected', user_mobile).attr('usertype');
        if ( usertype ==  'group'){
            $('#loadDataGroup').show();
            $('#uploaduser').hide();
        }else{
            $('#loadDataGroup').hide();
            $('#uploaduser').show();
        }

        loadGroup();
        $('select[name="user_mobile_id"], select[name="group_id"]').on('change', function (){
            loadGroup();
        });

        $('.userList').change(function () {
            var usertype = $('option:selected', this).attr('usertype');

            if ( usertype ==  'group'){
                $('#loadDataGroup').show();
                $('#uploaduser').hide();
            }else{
                $('#loadDataGroup').hide();
                $('#uploaduser').show();
            }
        });

        /* Tag Select */
        (function(){
            /* Limited */
            $(".tag-select-limited").chosen({
                max_selected_options: 5
            });

            /* Overflow */
            $('.overflow').niceScroll();
        })();

        $('button.btnSave').on('click', function (){
            groupForm();
        });

        $('.allchecbox').click(function (){
            if ( $(this).is(':checked')){
                $('input.checkbox').prop('checked', true);
            }else{
                $('input.checkbox').prop('checked', false);
            }
        });
    });

    function groupForm(){
        var user_mobile = $('select[name="user_mobile_id"]');
        var option = $('option:selected', user_mobile).attr('usertype');

        var user_mobile_id = user_mobile.val();
        if ( user_mobile_id == 0 ){
            alert("Please select User");
            return false;
        }

        var serirForm = $("#setupUserForm").serialize();
        $.ajax({
            type: "POST",
            url: "{{ route('setting.group.store') }}",
            async: true,
            //dataType: "json",
            data: serirForm,
            success: function(data) {
                alert('OK');
            }
        });
    }

    function allowChecked(frm){
        var sel = $(frm);
        $('input.checkbox').not(this).prop('checked', false);

        sel.prop('checked', true);
    }

    function loadGroup(){
        $('th input[type=checkbox]').prop('checked', false);

        var user_mobile = $('select[name="user_mobile_id"]');
        var usertype = $('option:selected', user_mobile).attr('usertype');

        if ( usertype == 'group' ) return false;

        var user_mobile_id = user_mobile.val();
        var group_id = $('select[name="group_id"]').val();
        var token 		= "{!! csrf_token() !!}";

        if ( user_mobile_id == 0 || group_id == 0 ){
            $('#loadData').empty();
            return false;
        }
        $.ajax({
            type: "POST",
            url: "{{ route('setting.group.load') }}",
            async: true,
            data: {_token: token, user_mobile_id : user_mobile_id, group_id : group_id} ,
            success: function(data) {
                $('#loadData').html(data.views);
                // if ( data['groupUserList'].length > 0 ){
                //     for( var j=0;j<data['groupUserList'].length;j++ ){
                //         $('.groupChild_'+data['groupUserList'][j].group_id+'_'+data['groupUserList'][j].user_num_id).iCheck('check');
                //     }
                // }
            }
        });

    }
</script>
@stop