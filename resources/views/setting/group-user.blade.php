@extends('layout.master')

@section('content')                                              
    <!-- Content -->
	<section id="content" class="container">
	    <!-- Breadcrumb -->
       	<ol class="breadcrumb hidden-xs">
			<li><a href="#">Home</a></li>
          	<li><a href="#">Library</a></li>
          	<li class="active">Data</li>
		</ol>
		<h4 class="page-title">Setup User</h4>
		<div class="listview list-container">
			<form method="post" id="setupUserForm">						
                <div class="block-area" id="custom-select">
                    <p>User</p>
                    <div class="row">
                        <div class="col-md-2 m-b-15">
                            <select class="select userList" name="user_mobile_id">
                                <option value="0">Select</option>
                                @if ( count($userMobile) > 0 )
                                    @foreach($userMobile as $um)
                                        <option value="{{ $um->id }}">{{ $um->name.' ['.$um->mobile.']' }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="clearfix"></div>

                        @if ( count($groups) > 0 )
                            <?php $count = 0;?>
                            @foreach($groups as $g)

                                <label class="groupsSelect" style="width: 120px;padding:0px;margin: 0px;">
                                    <input type="checkbox" name="group_id[{{ $g->id }}]" value="{{ $g->id }}" />
                                    <span style="font-size: 16px;font-weight: bold;">{{ $g->name }}</span>
                                </label>

                                <?php $count++?>
                                @if ( $count == 8 )
                                    <?php $count = 0;?>
                                    <div class="clearfix"></div>
                                    <div><hr class="whiter m-t-20" /></div>
                                @endif
                            @endforeach
                        @endif

                        <div class="clearfix"></div>
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-sm m-t-10 btnSave">Save</button>
                        <button type="button" class="btn btn-sm m-t-10">Cancel</button>
                    </div>
                </div>
			</form>
		</div>
	</section>
    <br /><br /><br /><br />
@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){

        loadGroup();
        $('select[name="user_mobile_id"]').on('change', function (){
            loadGroup();
        });

        /* Tag Select */
        (function(){
            /* Limited */
            $(".tag-select-limited").chosen({
                max_selected_options: 5
            });

            /* Overflow */
            $('.overflow').niceScroll();
        })();

        $('button.btnSave').on('click', function (){
            groupForm();
        });
    });

    function groupForm(){
        var user_mobile_id = $('select[name="user_mobile_id"]').val();
        if ( user_mobile_id == 0 ){
            alert("Please select User");
            return false;
        }

        var serirForm = $("#setupUserForm").serialize();
        $.ajax({
            type: "POST",
            url: "{{ route('setting.group.store') }}",
            async: true,
            //dataType: "json",
            data: serirForm,
            success: function(data) {
                alert('OK');
            }
        });
    }

    function loadGroup(){

        var user_mobile_id = $('select[name="user_mobile_id"]').val();
        var token 		= "{!! csrf_token() !!}";

        $('.groupsSelect input[type="checkbox"]').each(function (){
            $(this).iCheck('uncheck');
            var listeId = $(this).val();
            $('.listeChild'+listeId+' input[type="checkbox"]').iCheck('uncheck');
        });

        $.ajax({
            type: "POST",
            url: "{{ route('setting.group.load') }}",
            async: true,
            //dataType: "json",
            data: {_token: token, user_mobile_id : user_mobile_id} ,
            success: function(data) {

            }
        });

    }
</script>
@stop