<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head>
<body>
<button onclick="sendMessage()">Send message</button>
{!! Socket::javascript() !!}
<script>
    var socket = window.appSocket;

    function sendMessage() {
        var text = window.prompt('Which message would you like to send?');
        socket.send('sendMessageToOthers', text);
    }

    socket.on('newMessage', function (newMessage) {
        alert('New message: ' + newMessage);
    });

    socket.connect(function () {
        // The socket is connected.
    });
</script>
</body>
</html>