<!DOCTYPE html>
<html>
<head>
    <title>@yield('title', 'Backend - Report list')</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no">
    <meta charset="UTF-8">

    <meta name="description" content="Violate Responsive Admin Template">
    <meta name="keywords" content="Super Admin, Admin, Template, Bootstrap">

    <title>Super Admin Responsive Template</title>
        
    <!-- CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/calendar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/generics.css') }}" rel="stylesheet"> 
     <style type="text/css">
            .font-icons .icon { font-size: 25px; margin-right: 5px; }
            .font-icons [class*="col-"] { padding: 5px 10px; }
            .font-icons [class*="col-"]:hover { background: rgba(0,0,0,0.2); }
        </style>
    
</head>
<body>
    @yield('modals')
    
    @yield('styles')
    
    <!-- Content -->
    
    
    <body id="skin-blur-ocean">
    		@if ( !Auth::guest() )
        		@include('layout.top')
        @endif
        <section id="main" class="p-relative" role="main">
        		@if ( !Auth::guest() )
        			@include('layout.left')
        		@endif
        		@yield('content')
        </section>
    </div>        

    <script src="{{ asset('js/jquery.min.js') }}"></script> <!-- jQuery Library -->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script> <!-- jQuery UI -->
    <script src="{{ asset('js/jquery.easing.1.3.js') }}"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

	<!--  Form Related -->
    <script src="{{ asset('js/validation/validate.min.js') }}"></script> <!-- jQuery Form Validation Library -->
    <script src="{{ asset('js/validation/validationEngine.min.js') }}"></script> <!-- jQuery Form Validation Library - requirred with above js -->
    <script src="{{ asset('js/select.min.js') }}"></script> <!-- Custom Select -->
    <script src="{{ asset('js/chosen.min.js') }}"></script> <!-- Custom Multi Select -->
    <script src="{{ asset('js/datetimepicker.min.js') }}"></script> <!-- Date & Time Picker -->
    <script src="{{ asset('js/colorpicker.min.js') }}"></script> <!-- Color Picker -->
    <script src="{{ asset('js/icheck.js') }}"></script> <!-- Custom Checkbox + Radio -->
    <script src="{{ asset('js/autosize.min.js') }}"></script> <!-- Textare autosize -->
    <script src="{{ asset('js/toggler.min.js') }}"></script> <!-- Toggler -->
    <script src="{{ asset('js/input-mask.min.js') }}"></script> <!-- Input Mask -->
    <script src="{{ asset('js/spinner.min.js') }}"></script> <!-- Spinner -->
    <script src="{{ asset('js/slider.min.js') }}"></script> <!-- Input Slider -->
    <script src="{{ asset('js/fileupload.min.js') }}"></script> <!-- File Upload -->
    
    <!-- Charts -->
    <?php /*?>
    <script src="{{ asset('js/charts/jquery.flot.js') }}"></script> <!-- Flot Main -->
    <script src="{{ asset('js/charts/jquery.flot.time.js') }}"></script> <!-- Flot sub -->
    <script src="{{ asset('js/charts/jquery.flot.animator.min.js') }}"></script> <!-- Flot sub -->
    <script src="{{ asset('js/charts/jquery.flot.resize.min.js') }}"></script> <!-- Flot sub - for repaint when resizing the screen -->

    <script src="{{ asset('js/sparkline.min.js') }}"></script> <!-- Sparkline - Tiny charts -->
    <script src="{{ asset('js/easypiechart.js') }}"></script> <!-- EasyPieChart - Animated Pie Charts -->
    <script src="{{ asset('js/charts.js') }}"></script> <!-- All the above chart related functions -->
    <?php */?>
    
    <!-- Map -->
    <?php /*?>
    <script src="{{ asset('js/maps/jvectormap.min.js') }}"></script> <!-- jVectorMap main library -->
    <script src="{{ asset('js/maps/usa.js') }}"></script> <!-- USA Map for jVectorMap -->
    <?php */?>
    <!--  Form Related -->
    <script src="{{ asset('js/icheck.js') }}"></script> <!-- Custom Checkbox + Radio -->

    <!-- UX -->
    <script src="{{ asset('js/scroll.min.js') }}"></script> <!-- Custom Scrollbar -->

    <!-- Other -->

    <!-- All JS functions -->
    <script src="{{ asset('js/functions.js') }}"></script>

    <script>
        $(function(){
            $('table.table-bordered tbody tr').on('mouseover', function(){
                $(this).css('background', '#bdd2ff');
            }).on('mouseleave', function(){
                $(this).css('background', '');
            });
        });

        function loadHover(){
            $('table.table-bordered tbody tr').on('mouseover', function(){
                $(this).css('background', '#bdd2ff');
            }).on('mouseleave', function(){
                $(this).css('background', '');
            });
        }
    </script>

    @yield('scripts')
</body>
</html>
