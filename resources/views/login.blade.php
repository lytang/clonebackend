@extends('layout.master')

@section('content')


<div class="clearfix"></div>

<div class="container">
	<div style="height: 50px;">&nbsp;</div>
	<div class="row">
			<div class="panel panel-default">
				<div style="text-align: center"><h2>Login</h2></div>
				<div>
					@if(Session::get('loginError'))
	                    <div class="alert alert-danger">
	                        <p align="center">{{ Session::get('loginError') }}</p>
	                    </div>
                    @endif
                    @if(Session::get('success'))
	                    <div class="alert alert-success">
	                        <p align="center">{{ Session::get('success') }}</p>
	                    </div>
                    @endif

					<form class="form-horizontal" role="form" method="POST" action="{{ route('authent') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Username</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="username" value="{{ old('username') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-alt m-r-5">Login</button>

<!-- 								<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a> -->
							</div>
						</div>
					</form>
				</div>
		</div>
	</div>
</div>
@endsection
