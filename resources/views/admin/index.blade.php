@extends('layout.master')

@section('content')                                              
    <!-- Content -->
	<section id="content" class="container">
	    <!-- Breadcrumb -->
       	<ol class="breadcrumb hidden-xs">
			<li><a href="#">Home</a></li>
          	<li><a href="#">Library</a></li>
          	<li class="active">Data</li>
		</ol>
            
		<h4 class="page-title">User List</h4>
		<div class="listview list-container">
			<header class="listview-header media">

				<ul class="list-inline list-mass-actions pull-left">
                		<li><a data-toggle="modal" href="{{ route('admin.user.create') }}" title="Add" class="tooltips"><i class="sa-list-add"></i></a></li>
					<li><a href="#" title="Refresh" class="tooltips"><i class="sa-list-refresh"></i></a></li>
					<li class="show-on" style="display: none;"><a href="#" title="Move" class="tooltips"><i class="sa-list-move"></i></a></li>
					<li class="show-on" style="display: none;"><a href="#" title="Delete" class="tooltips"><i class="sa-list-delete"></i></a></li>
				</ul>

				<input class="input-sm col-md-4 pull-right message-search" type="text" placeholder="Search....">
                        
				<div class="clearfix"></div>
			</header>
			
			<!-- Bordered -->
			<div class="block-area" id="tableBordered">            		
                <div class="table-responsive overflow">
                    <table class="table table-bordered tile">
                    <thead>
                        <tr>
                            <th width="50">No.</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th width="100"></th>
                        </tr>
                    </thead>
                    <tbody>
                    		<?php $count = 1?>
                    		@if ( count($userMobile) > 0 )
                    			@foreach($userMobile as $u)
                    				<tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $u->name }}</td>
                                    <td>{{ $u->mobile }}</td>
                                    <td>                            		
                                        <a href="{{ route('admin.user.edit', [$u->id]) }}"><span class="icon">&#61952;</span></a> |
                                        <a href="{{ route('admin.user.delete', [$u->id]) }}"><span class="icon">&#61918;</span></a>
                                    </td>
                                </tr>	
                                <?php $count++?>
                    			@endforeach
                    		@endif                      
                    </tbody>
                    </table>
                </div>    
            </div>
			
		</div>
	</section>                
@stop