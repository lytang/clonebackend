@extends('layout.master')

@section('content')                                              
    <!-- Content -->
	<section id="content" class="container">
	    <!-- Breadcrumb -->
       	<ol class="breadcrumb hidden-xs">
			<li><a href="#">Home</a></li>
          	<li><a href="#">Library</a></li>
          	<li class="active">Data</li>
		</ol>
            
		<h4 class="page-title">User Create</h4>
		<div class="listview list-container">

            <div class="col-md-12">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
			<div class="tile p-15">
				<form role="form" method="post" action="{{ route('admin.user.store') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="col-md-3">
                        <label for="InputEmail1">Name</label>
                        <input type="text" name="name" class="form-control input-sm" value="{{ $userMobile != null ? $userMobile->name : '' }}" id="InputEmail1" placeholder="Enter Name">
                    </div>
                    	                    	                    
                    <div class="col-md-3">
                        <label for="InputPhone">Phone [012456789]</label>
                        <input type="tel" {{ $userMobile != null ? 'readonly=readonly' : ""}} name="mobile" value="{{ $userMobile != null ? $userMobile->mobile : '' }}" class="form-control input-sm m-b-10" placeholder="Phone Number">
                    </div>
                    <div class="col-md-3">
                        <label for="InputPhone">ប្រភេទ</label>
                        <select name="types" class="form-control select2 input-sm m-b-10">
                            <option value="upload">Upload</option>
                            <option value="group">Group</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <input type="hidden" name="id" value="{{ isset($userMobile) && $userMobile != null ? $userMobile->id : 0 }}" />
                    <button type="submit" class="btn btn-sm m-t-10">Save</button>
                    <button type="button" class="btn btn-sm m-t-10 cancel">Cancel</button>
                </form>
            </div>
		</div>
	</section>


@stop

@section('scripts')
<script>
    $(function(){
        $('.cancel').click(function(){
            window.location = "{{ route('admin.index') }}";
        });
    }) ;
</script>
@stop